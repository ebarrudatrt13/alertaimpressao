﻿$JobImpressao = @()

$logimpressao = Get-WinEvent -FilterHashTable @{LogName="Microsoft-Windows-PrintService/Operational"; ID=307}


  $Atributo = [xml]$logimpressao[0].ToXml()
  $PrinterName = $Atributo.Event.UserData.DocumentPrinted.Param5
    
  $Detalhes = New-Object -TypeName psobject -Property @{
      DataHora = $logimpressao[0].TimeCreated
	  Estacao = $Atributo.Event.UserData.DocumentPrinted.Param4
	  Usuario = $Atributo.Event.UserData.DocumentPrinted.Param3
      NomeDoc = $Atributo.Event.UserData.DocumentPrinted.Param2
      Impressora = $PrinterName
      Paginas = $Atributo.Event.UserData.DocumentPrinted.Param8
    }

    $JobImpressao += $Detalhes


# Exportar pra um CSV	
	$JobImpressao  | Export-Csv -Append -LiteralPath C:\Temp\$estacao.csv
	
<# Incluir variáveis na URL de atualização.
 	1.Retirar os \\ do nome da estação	
		($JobImpressao.Estacao).SubString(2)
	2.Formatar data pro formato do MySQL
		($JobImpressao.DataHora).toString("yyyy-MM-dd H:MM:s") #>

$urlrequest = 'http://10.13.x.y:9090/coleta_trabalho_impressao.php?pc=' + ($JobImpressao.Estacao).SubString(2) + '&usuario=' + $JobImpressao.usuario + '&impressora=' + $JobImpressao.impressora + '&nomedoc=' + $JobImpressao.NomeDoc + '&pag=' + $JobImpressao.Paginas + '&datahora=' + ($JobImpressao.DataHora).toString("yyyy-MM-dd H:mm:s")

# Enviar o Request pro servidor
Invoke-WebRequest $urlrequest