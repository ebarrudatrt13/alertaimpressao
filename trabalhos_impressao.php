 <?php
$servername = "10.13.x.y";
$username = "usuario_apoio";
$password = "senha";
$dbname = "impressao";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$sql = "SELECT id, datahora, estacao, usuario, nome_documento, impressora, qtde_paginas FROM trabalho";
$result = mysqli_query($conn, $sql);

echo "<table style='text-align: left; width: 100%;' border='1' cellpadding='2' cellspacing='2'>" .
"<tbody>" .
"<tr>" .
"<td style='vertical-align: top;'>ID</td>" .
"<td style='vertical-align: top;'>ESTAÇÃO</td>" .
"<td style='vertical-align: top;'>USUÁRIO</td>" .
"<td style='vertical-align: top;'>IMPRESSORA</td>" .
"<td style='vertical-align: top;'>DOCUMENTO</td>" .
"<td style='vertical-align: top;'>QTDE PÁGINAS</td>" .
"<td style='vertical-align: top;'>DATA/HORA</td>" .
"</tr>";

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
echo "<tr>" .
"<td style='vertical-align: top;'>" . $row["id"]. "</td>" .
"<td style='vertical-align: top;'>" . $row["estacao"]. "</td>" .
"<td style='vertical-align: top;'>" . $row["usuario"]. "</td>" .
"<td style='vertical-align: top;'>" . $row["impressora"]. "</td>" .
"<td style='vertical-align: top;'>" . $row["nome_documento"]. "</td>" .
"<td style='vertical-align: top;'>" . $row["qtde_paginas"]. "</td>" .
"<td style='vertical-align: top;'>" . $row["datahora"]. "</td>" .
"</tr>";
    }
} else {
    echo "0 results";
}

mysqli_close($conn);
echo "</tbody></table>";
?> 

