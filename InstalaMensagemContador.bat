
@echo off

REM Contador

C:\windows\system32\wevtutil.exe sl Microsoft-Windows-PrintService/Operational /e:true

copy printaudit.ps1 C:\Users\Public\Documents /Y
copy PrintAudit.xml C:\Users\Public\Documents /Y

C:\windows\system32\schtasks.exe /delete /tn "Contador de impressao" /f
C:\windows\system32\schtasks.exe /Create /XML "C:\Users\Public\Documents\PrintAudit.xml" /ru SYSTEM /TN "Contador de impressao"

echo %date%;%time%;contador-impressao >> C:\Software\landesk.csv

REM Mensagem

C:\windows\system32\wevtutil.exe sl Microsoft-Windows-PrintService/Operational /e:true

copy ExibirMensagem.xml C:\Users\Public\Documents /Y

copy msgprn.txt C:\Users\Public\Documents /Y
copy msgprn.bat C:\Users\Public\Documents /Y

c:\windows\system32\schtasks.exe /delete /tn "Exibir alerta de impressao" /f

c:\windows\system32\schtasks.exe /Create /XML "c:\Users\Public\Documents\ExibirMensagem.xml" /ru SYSTEM /TN "Exibir alerta de impressao"

echo %date%;%time%;alerta-impressao >> C:\Software\landesk.csv
